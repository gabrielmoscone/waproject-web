export default interface IOrderToken {
  id: number;
  description: string;
  amount: number;
  value?: number;
  roles: string[];
  exp: number;
}
